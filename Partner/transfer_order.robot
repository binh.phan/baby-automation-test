*** Settings ***
Resource     ../front_end/resource.robot
# Resource    ../common/common.robot
#Suite Setup   Open Browser To Login Page
Test Teardown     Close All Browsers
Suite Teardown    Close All Browsers


*** Test Cases ***

Transfer Order
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[8]
   Sleep    5s
   Select Checkbox    xpath://*[@id="main-wrapper"]/div[4]/div/div[3]/div/div/div/form/div[3]/div[4]/input
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[3]/div/div/div/form/div[4]/div/button
   Sleep    5s
   Wait Until Page Contains   Transfer order to selected partners successfully!

Another Partner accept Order
   Login As Partner    partner1@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Wait Until Page Contains     Doremonhokkaido
   Sleep    5s
   Click Element    xpath://*[@id="transferslist"]/tbody/tr/td[1]/a
   Sleep    5s
   Click Element    xpath://*[@id="paymentBtn"]
   Sleep   5s
   Wait Until Page Contains    Transferred Successfully!