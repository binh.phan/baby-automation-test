*** Settings ***
Resource     ../front_end/resource.robot
# Resource    ../common/common.robot
#Suite Setup   Open Browser To Login Page
Test Teardown     Close All Browsers
Suite Teardown    Close All Browsers


*** Test Cases ***

Add A New Item
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element    xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[1]
   Sleep   5s
   Select Checkbox    name=items[]
   Click Element   xpath://*[@id="paymentBtn"]
   Sleep   5s
   Click Element   xpath://*[@id="submitBtn3"]
   Click Element   xpath://*[@id="payConfirm"]
   Sleep   5s
   Wait Until Page Contains    Items were added and payment was a success!

Add A New Charge
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[4]
   Sleep    5s
   Input Text   xpath://*[@id="desc"]    add new Charge
   Input Text   xpath://*[@id="amount"]   5
   Click Element   xpath://*[@id="submitBtn"]
   Sleep   5s
   Click Element   xpath://*[@id="submitBtn3"]
   Click Element   xpath://*[@id="payConfirm"]
   Sleep   5s
   Wait Until Page Contains    New charge was added and payment was a success!


Add A New Delivery
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[5]
   Sleep   5s
   Input Text   xpath://*[@id="desc"]   Add a new charge
   Input Text   xpath://*[@id="amount"]    5
   Click Element   xpath://*[@id="submitBtn"]
   Sleep   5s
   Click Element   xpath://*[@id="submitBtn3"]
   Click Element   xpath://*[@id="payConfirm"]
   Sleep   5s
   Wait Until Page Contains     New delivery fee was added and payment was a success!

Check The Add Information
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="ui-id-3"]
   Wait Until Page Contains      $64.86
   Wait Until Page Contains      Added New Charge: add new Charge for $5.79
   Wait Until Page Contains      Added New Delivery Fee: Add a new charge for $5.79


Change Rental Date To Shorter
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[7]
   Sleep    5s
   Clear Element Text   id=rental_end_day
   Sleep   5s
   Input Text   id=rental_end_day   09/15/2019
   Press Key    id=rental_end_day   \\9
   Sleep    5s
   Click Element   xpath://*[@id="submitBtn"]
   Click Element   xpath://*[@id="submit"]
   Sleep    5s
   Wait Until Page Contains    Shorten rental successfully

Check Refund Amount After Change Date To Shorter
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Wait Until Page Contains      $48.65
   Click Element   xpath://*[@id="ui-id-3"]
   Sleep   5s
   Wait Until Page Contains     $9.27

Change Rental Date To Longer
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[7]
   Sleep    5s
   Clear Element Text   id=rental_end_day
   Sleep   5s
   Input Text   id=rental_end_day   09/25/2019
   Press Key    id=rental_end_day   \\9
   Sleep    5s
   Click Element   xpath://*[@id="submitBtn"]
   Click Element   xpath://*[@id="submit"]
   Sleep    5s
   Click Element   xpath://*[@id="submitBtn3"]
   Click Element   xpath://*[@id="payConfirm"]
   Wait Until Page Contains      Extend rental successfully!

Check Amount After Add More Date
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="ui-id-3"]
   Sleep   5s
   Wait Until Page Contains    Change Rental Dates from 09/10/2019 - 09/15/2019 to 09/10/2019 - 09/25/2019 for $579.14

Refund An Item
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[2]
   Sleep   5s
   Select Checkbox   name=refunds[]
   Click Element   xpath://*[@id="submitBtn"]
   Click Element   xpath://*[@id="submit"]
   Sleep   5s
   Wait Until Page Contains    Item(s) were refunded and payment refund was a success!


Refund Delivery
   Login As Partner    partner@demo.com    Password1234@
   Sleep   5s
   Click Element   xpath://*[@id="menu"]/li[3]/a/span
   Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
   Sleep    5s
   Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
   Sleep   5s
   Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[6]
   Sleep   5s
   Input Text   xpath://*[@id="desc"]   Refund items
   Input Text   xpath://*[@id="amount"]    5
   Click Element   xpath://*[@id="submitBtn"]
   Click Element   xpath://*[@id="submit"]
   Sleep    5s
   Wait Until Page Contains     Delivery fee were refunded and payment refund was a success!




