*** Settings ***
Resource     ../front_end/resource.robot
# Resource    ../common/common.robot
#Suite Setup   Open Browser To Login Page
Test Teardown     Close All Browsers
Suite Teardown    Close All Browsers


Cancel Reservation
    Login As Partner    partner@demo.com    Password1234@
    Sleep   5s
    Click Element   xpath://*[@id="menu"]/li[3]/a/span
    Click Element  xpath://*[@id="menu"]/li[3]/ul/li[1]/a
    Sleep    5s
    Click Element   xpath://*[@id="orderslist"]/tbody/tr[1]/td[1]/a
    Sleep   5s
    Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div[1]/span[3]/a
    Sleep   5s
    Click Element   xpath://*[@id="main-wrapper"]/div[4]/div/div[2]/div/div/div/div/a[3]
    Sleep   5s
    Click Element   xpath://*[@id="submitBtn"]
    Click Element    xpath://*[@id="submit"]
    Wait Until Page Contains    The Reservation was cancelled.