# Baby Automation Test

0. Make sure Python is installed:
> python --version
Python 3.6.0

1. Install Robot Framework
  python -m pip install robotframework

2. Install selenium Library
  python -m pip install --upgrade robotframework-seleniumlibrary

3. Browser drivers
For testing with Firefox, install geckodriver from https://github.com/mozilla/geckodriver/releases

For testing with Chrome, install chromedriver from https://sites.google.com/a/chromium.org/chromedriver/downloads

For testing with Edge, install Edge webdriver from https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/

Any browser drivers must be on PATH !


Help:
Selenium Keywords:  http://robotframework.org/Selenium2Library/Selenium2Library.html

To run all tests:

robot resource