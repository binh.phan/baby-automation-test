*** Settings ***
Library           SeleniumLibrary

*** Variable ***
${URL}   https://www.babierge.tk/
${URL1}   https://www.babierge.tk/laura1
${URL2}   https://www.babierge.tk/partner
${BROWSER}  Firefox

*** Keywords ***
Open Browser To Home Page
  Open Browser  ${URL}  ${BROWSER}
  Maximize Browser Window

Open Browser To Order
  Open Browser   ${URL1}   ${BROWSER}
  Maximize Browser Window

Open Browser For Partner
  Open Browser   ${URL2}   ${BROWSER}
  Maximize Browser Window

Input Username Partner
  [Arguments]   ${username}
  Input Text  id=username   ${username}

Input Password Partner
  [Arguments]   ${password}
  Input Text  id=password  ${password}

Login As Partner
  [Arguments]   ${username}   ${password}
  Open Browser For Partner
  Input Username Partner  ${username}
  Input Password Partner  ${password}
  Click Element   xpath://*[@id="top"]/div[1]/div[2]/div[2]/form/fieldset/div[3]/input
  Wait Until Page Contains   You have successfully logged in.

Input Pickup Information
   [Arguments]   ${address}   ${city}   ${zipcode}
   Click Element   xpath://*[@id="hs-eu-confirmation-button"]
   Click Element   xpath://*[@id="plaura1_i2380"]/form/div[2]/button
   Sleep    5s
   Click Element   xpath://*[@id="plaura1_i2379"]/form/div[2]/button
   Sleep   5s
   Click Element   xpath://*[@id="topnavigation"]/div[5]/a/span[2]
   Sleep    5s
   Click Element   xpath:/html/body/div[3]/footer/a
   Click Element   xpath:/html/body/article/div/div/div[1]/div[2]/div[1]/a/button
   Sleep   5s
   Click Element   id=delivery_option_id
   Sleep   5s
   Select From List By Label   id=delivery_option_id    10-20 Miles from Universal City, TX $35
   Sleep   5s
   Click Element   id=rental_type
   Select From List By Value   id=rental_type     Private Residence
   Input Text   id=address    ${address}
   Input Text   id=city    ${city}
   Input Text   id=zip_province     ${zipcode}
   Input Text   id=rental_start_day    09.10.2019
   Input Text   id=rental_end_day    09.16.2019
   Click Element   xpath://*[@id="delivery"]/div[2]/footer/button
   Sleep    5s
Input Customer Information
   [Arguments]  ${first_name}  ${last_name}  ${email}
   Input Text   id=customer_first_name    ${first_name}
   Input Text   id=customer_last_name     ${last_name}
   Input Text   id=customer_email    ${email}
   Input Text   id=customer_phone    0987222222
   Click Element  id=customer_referral
   Select From List By Value   id=customer_referral     Internet Search
   Click Element  xpath://*[@id="contact"]/div[2]/footer/button
   Sleep   8s
Input Card Number
   [Arguments]   ${card_number}
   Wait Until Page Contains Element    xpath://*[@id="card-number"]/div/iframe
   Select Frame    xpath://*[@id="card-number"]/div/iframe
   Input Text    name=cardnumber    ${card_number}
   Sleep   2s
   Unselect Frame
   Select Frame    xpath://*[@id="card-expiry"]/div/iframe
   Input Text    name=exp-date   0224
   Unselect Frame
   Select Frame    xpath://*[@id="card-cvc"]/div/iframe
   Input Text    name=cvc  123
   Sleep    5s
   Unselect Frame
   Click Element    xpath://*[@id="billing"]/div[2]/fieldset[2]/div[1]/section/label
   Click Element    xpath://*[@id="billing"]/div[2]/footer/button
   Sleep    10s

