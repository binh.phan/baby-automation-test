*** Settings ***
Resource      resource.robot
# Resource    ../common/common.robot
#Suite Setup   Open Browser To Login Page
Test Teardown     Close All Browsers
Suite Teardown    Close All Browsers


*** Test Cases ***

Make An Order Successfully
   Open Browser To Order
   Sleep   5s
   Input Pickup Information   403 NTB   Ho Chi Minh   12345
   Input Customer Information   Doremon   Hokkai    kiet.phan@hdwebsoft.co
   Input Card Number    4242424242424242
   Wait Until Page Contains    Thank you for your reservation

# Make An Order Fail With Blanh zipcode
#   Open Browser To Order
#   Sleep   5s
#   Input Pickup Information   403 NTB   Ho Chi Minh   ${EMPTY}
#   Wait Until Page Contains     This field is required

# Make An Order Fail With invalid Card Number
#    Open Browser To Order
#    Sleep   5s
#    Input Pickup Information   403 NTB   Ho Chi Minh   12345
#    Input Customer Information   Doremon   Hokkai    kiet.phan@hdwebsoft.co
#    Input Card Number    4200000000000000
#    Wait Until Page Contains    Error: Your card was declined. Your request was in test mode, but used a non test (live) card. For a list of valid test cards, visit: https://stripe.com/docs/testing.














